<?php
require 'vendor/autoload.php';

session_start();



$db_name = config('database.dbName');
$db_user = config('database.dbUser');
$db_db_password = config('database.dbPass');
$pdo = new PDO('mysql:dbname=' . $db_name, $db_user, $db_db_password);
$instance = new \Envms\FluentPDO\Query($pdo);

Flight::Set('db', $instance);

Flight::set('flight.log_errors', true);






Flight::route('/running-process', ['JobController', 'runningProcess']);
/**
 * template
 */
Flight::route('/', ['TemplateController', 'index']);
Flight::route('/sample', ['JobController', 'sample']);
Flight::route('/template', ['TemplateController', 'index']);
Flight::route('GET /template/new', ['TemplateController', 'newTemplate']);
Flight::route('POST /template/new', ['TemplateController', 'createNewTemplate']);
/**
 * Job
 */
Flight::route('GET /single-generate', ['RouteController', 'generateSingle']);
Flight::route('POST /single-generate', ['RouteController', 'doGenerateSingle']);
Flight::route('GET /single-generate/@slug', ['RouteController', 'generateSingleHtml']);

Flight::route('GET /inject', ['RouteController', 'inject']);
Flight::route('POST /inject', ['RouteController', 'doInject']);

Flight::route('GET /delete-confirm/@slug', ['RouteController', 'deleteConfirm']);
Flight::route('POST /delete-confirm/@slug', ['RouteController', 'doDeleteJob']);

Flight::route('GET /delete-generate-confirm/@slug', ['RouteController', 'doDeleteJobGenerate']);

Flight::route('/finished', ['RouteController', 'finished']);

Flight::route('GET /job-details/@slug', ['RouteController', 'jobDetails']);
Flight::route('GET /job-export/@slug(/@type)', ['RouteController', 'jobExport']);
Flight::route('POST /job-details/@slug', ['RouteController', 'doRunJobDetails']);

/**
 * ajax
 */
Flight::route('GET /ajax/get-blade-syntax/@slug', ['AjaxController', 'getBladeSyntax']);
Flight::route('POST /ajax/post-syntax', ['AjaxController', 'postBladeSyntax']);


// Flight::map('error', function(Exception $ex){
//   // Handle error
//   echo $ex->getTraceAsString();
// });


Flight::route('GET /auth/login', ['AuthController', 'login']);
Flight::route('POST /auth/login', ['AuthController', 'doLogin']);
Flight::route('/auth/logout', ['AuthController', 'runningProcess']);

// Flight::redirect('/auth/login');



// check auth



Flight::start();
