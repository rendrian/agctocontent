<div class="joblist">
  <table id="job-list">
    <thead>
      <tr>
        <th class="num">#</th>
        <th class="name">Name</th>
        <th class="status">Status</th>
        <th class="aksi">Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($joblist as $key => $item)

        <tr>
          <td class="num">{{ $key + 1 }}</td>
          <td class="name"> <a href="/job-details/{{ $item['id'] }}"> » {{ $item['name'] }}</a></td>

          @switch($item['ready'])
            @case(0)
              <td class="status danger">Loading</td>
            @break
            @case(1)
              <td class="status warning">Scrape Ready</td>
            @break
            @case(2)
              <td class="status success">Export Ready</td>
            @break
            @case(3)
              <td class="status warning">Exporting</td>
            @break
            @case(4)
              <td class="status download-now">
                <a target="_blank" href="/job/finished/{{ $item['xml_file'] }}">Download Now</a>
              </td>
            @break
            @default

          @endswitch


          <td class="aksi">
            {{-- <a href="/job-details/{{ $item->id }}">
              »DETAILS
            </a> --}}
            <a href="/job-export/{{ $item['id'] }}/wp" class="to-blogger">
              »BlOGGER
            </a>
            <a href="/job-export/{{ $item['id'] }}/blogger" class="to-wp">
              »WP
            </a>

            <a href="/job/data/{{ $item['id'] }}/logs.txt" target="_blank">
              »LOG
            </a>

            <a href="/delete-confirm/{{ $item['id'] }}">
              »DELETE
            </a>

          </td>
        </tr>
      @endforeach
    </tbody>
  </table>

</div>


@push('footer')
  <script src="/assets/js/jquery.js"></script>
  {{-- <link rel="stylesheet" href="//cdn.datatables.net/1.11.1/css/jquery.dataTables.min.css"> --}}
  <script src="/assets/js/jquery.dataTables.min.js"></script>
  <script>
    $(document).ready(function() {
      $('#job-list').DataTable();
    });
  </script>
@endpush
