@extends('admin.layout')

@section('content')
  <div class="delete-confirm">

    <form action="" method="POST">
      <input type="hidden" name="{{ $slug }}">
      <p>
        Delete this job ?
      </p>
      <p>
        <strong>{{ $slug }}</strong>
      </p>


      <input type="submit" value="DELETE">
    </form>

  </div>


@endsection
