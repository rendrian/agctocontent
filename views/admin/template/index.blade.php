@extends('admin.layout')
@section('title', 'Add and Modify Template For Generated Content')

@section('content')

  <h2>Monaco Editor Sync Loading Sample</h2>
  <div class="form-control">
    <label for="template">Select Template</label>
    <select name="template" id="template">
      @foreach (templateList() as $item)
        <option value="{{ $item }}">{{ $item }}</option>
      @endforeach
    </select>
  </div>
  <a href="/template/new"> <button class="btn-block" id="new-template">NEW TEMPLATE</button></a>

  <pre id="container" cols="30" rows="10" style="width: 100%; height: 600px;"></pre>


  <button class="btn-block" id="save-it">SAVE</button>


@endsection


@push('css')

  <link rel="stylesheet" data-name="vs/editor/editor.main"
    href="/node_modules/monaco-editor/min/vs/editor/editor.main.css" />

  <script src="/assets/js/jquery.js"></script>
  <script src="/assets/plugins/jquery-loading-overlay/loadingoverlay.min.js"></script>
@endpush


@push('js')
  <script>
    var require = {
      paths: {
        vs: '/node_modules/monaco-editor/min/vs'
      }
    };
  </script>
  <script src="/node_modules/monaco-editor/min/vs/loader.js"></script>
  <script src="/node_modules/monaco-editor/min/vs/editor/editor.main.nls.js"></script>
  <script src="/node_modules/monaco-editor/min/vs/editor/editor.main.js"></script>

  <script>
    function showLoading() {
      $("#container").LoadingOverlay("show", {
        image: "/assets/icons/loading-money.svg",
        text: "Nunggu yaaah!! :)",
        textColor: "#ffffff",
        background: "rgb(10 10 10 / 40%)"
      });
    }

    function hideLoading() {
      $("#container").LoadingOverlay("hide");
    }
    var editor = monaco.editor.create(document.getElementById('container'), {
      theme: 'vs-dark',
      language: 'html'
    });
    setTimeout(function() {
      editor.updateOptions({
        lineNumbers: "on"
      });
    }, 2000);

    $(document).ready(function() {
      var selectedTemplate = $('#template').val();
      getValue();
      showLoading();

      function getValue() {
        $.ajax({
          url: "/ajax/get-blade-syntax/" + selectedTemplate,
          context: document.body
        }).done(function(data) {
          editor.setValue(data.data);
          hideLoading();

        });

      }

    });

    function getValue() {
      showLoading();
      $.ajax({
        url: "/ajax/get-blade-syntax/" + selectedTemplate,
        context: document.body
      }).done(function(data) {
        editor.setValue(data.data);
        hideLoading();

      });

    }

    $('#save-it').on('click', function() {
      showLoading();
      selectedTemplate = $('#template').val();
      $.ajax({
        url: "/ajax/post-syntax",
        type: "POST",
        data: {
          template: selectedTemplate,
          syntax: window.editor.getValue()
        }

      }).done(function(data) {
        if (data.success == true) {
          alert('data saved');
        }
        hideLoading();

      });
    });
    $('#template').on('change', function() {
      selectedTemplate = this.value;
      getValue();
    });
  </script>




@endpush
