@extends('admin.layout')
@section('title','Add New Template For Generated Content')

@section('content')
  <h2 class="content-title">Create New Template</h2>
  <form action="" method="post">
    <div class="form-control">
      <label for="template_name">Template Name</label>
      <input type="text" name="template_name">
    </div>
    <input type="submit" class="btn-block" value="CREATE TEMPLATE">
  </form>
@endsection
