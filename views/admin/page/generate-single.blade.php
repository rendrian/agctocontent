@extends('admin.layout')
@section('title', 'Generate Single Content From Keyword')
@section('content')
  <form action="" method="POST">

    <div class="form-control">
      <label for="append">Prepend</label>
      <input type="text" class="btn-block" name="prepend" autocomplete="off" placeholder="Tambahan sebelum keyword"
        value="{{ isset($_GET['prepend']) ? $_GET['prepend'] : '' }}">
    </div>
    <div class="form-control">
      <label for="append">Append</label>
      <input type="text" class="btn-block" name="append" autocomplete="off" placeholder="Tambahan setelah keyword"
        value="{{ isset($_GET['append']) ? $_GET['append'] : '' }}">
    </div>
    <div class="form-control">
      <label for="template">Template</label>
      <select name="template" id="template">
        @foreach (templateList() as $item)
          @php
            $selected = isset($_GET['template']) ? $_GET['template'] : '';
          @endphp
          <option value="{{ $item }}" {{ trim($selected) == trim($item) ? 'selected' : '' }}>
            {{ $item }}</option>
        @endforeach
      </select>
    </div>
    <div class="form-control">
      <label for="template">IS PDF</label>
      <select name="is_pdf" id="is_pdf">
        <option value="0">FALSE</option>
        <option value="1">TRUE</option>

      </select>
    </div>
    <div class="form-control">
      <label for="keywordlist">Keywords</label>
      <input type="text" class="btn-block" name="keyword" autocomplete="off"
        placeholder="ex : Cara memakai dasi yang baik" value="{{ isset($_GET['keyword']) ? $_GET['keyword'] : '' }}">

    </div>
    <input type="submit" class="btn-block" value="INJECT">
  </form>

  <button class="btn-block" onclick="window.location.reload();">REFRESH</button>



  <table>
    <thead>
      <tr>
        <th class="num">#</th>
        <th class="name">Keyword</th>
        <th class="status">Status</th>
        <th class="aksi">Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($joblist as $key => $item)

        <tr>
          <td class="num">{{ $key + 1 }}</td>
          <td class="name"> <a
              href="/single-generate/{{ $item->id }}">{{ $item->prepend . ' ' . $item->keyword . ' ' . $item->append }}</a>
          </td>

          @switch($item->status)
            @case(0)
              <td class="status warning">Generating</td>
            @break
            @case(1)
              <td class="status download-now">
                <a target="_blank" href="/single-generate/{{ $item->id }}">GENERATE HTML</a>
              </td>
            @break

            @default

          @endswitch


          <td class="aksi">
            <a href="/delete-generate-confirm/{{ $item->id }}">
              »DELETE
            </a>

          </td>
        </tr>
      @endforeach
    </tbody>
  </table>

@endsection

@push('css')

  <link rel="stylesheet" data-name="vs/editor/editor.main"
    href="/node_modules/monaco-editor/min/vs/editor/editor.main.css" />

  <script src="/assets/js/jquery.js"></script>
  <script src="/assets/plugins/jquery-loading-overlay/loadingoverlay.min.js"></script>
@endpush


@push('js')

  <script>
    $(document).ready(function() {


    });


    $('#prepend,#append,#keywords').on('change', function() {
      console.log('ganti')
    });
  </script>




@endpush
