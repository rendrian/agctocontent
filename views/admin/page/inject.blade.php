@extends('admin.layout')
@section('title', 'Generate XML From Keyword')
@section('content')
  <form action="/inject" method="POST">
    <div class="form-control">
      <label for="name">Name</label>
      <input type="text" class="btn-block" name="name" autocomplete="off">
    </div>
    <div class="form-control">
      <label for="append">Prepend</label>
      <input type="text" class="btn-block" name="prepend" autocomplete="off">
    </div>
    <div class="form-control">
      <label for="append">Append</label>
      <input type="text" class="btn-block" name="append" autocomplete="off">
    </div>
    <div class="form-control">
      <label for="template">Template</label>
      <select name="template" id="template">
        @foreach (templateList() as $item)
          <option value="{{ $item }}">{{ $item }}</option>
        @endforeach
      </select>
    </div>
    <div class="form-control">
      <label for="template">IS PDF</label>
      <select name="is_pdf" id="is_pdf">
        <option value="false">FALSE</option>
        <option value="true">TRUE</option>

      </select>
    </div>
    <div class="form-control">
      <label for="keywordlist">Keywords</label>
      <textarea name="keywordlist" id="" cols="30" rows="10"></textarea>
    </div>
    <input type="submit" class="btn-block" value="INJECT">
  </form>


  @includeIf('admin.job.list',['joblist'=>$joblist])

@endsection
