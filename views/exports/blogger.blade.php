{!! '<' . '?' . "xml version='1.0' encoding='UTF-8'?>" !!}
<ns0:feed xmlns:ns0="http://www.w3.org/2005/Atom">
  <ns0:title type="html">wpan.com</ns0:title>
  <ns0:generator>Blogger</ns0:generator>
  <ns0:link href="http://localhost/wpan" rel="self" type="application/atom+xml" />
  <ns0:link href="http://localhost/wpan" rel="alternate" type="text/html" />
  <ns0:updated>2016-06-10T04:33:36Z</ns0:updated>
  @php
    $time = new DateTime(date('c'));
    $timeToAdd = 2;
  @endphp

  @foreach ($data as $post)
    @php
      $id = rand(123123, 17263813);
      
      $keywords = explode(PHP_EOL, $post['config']->keywords);
      
      shuffle($keywords);
      $time->add(new DateInterval('PT' . $timeToAdd . 'H'));
      $date = $time->format('c');
      
    @endphp
    <ns0:entry>
      @foreach ($keywords as $key => $config)
        @if ($key < 5)
          <ns0:category scheme="http://www.blogger.com/atom/ns#" term="{{ limit_the_words($config, 3) }}" />
        @endif
      @endforeach

      <ns0:category scheme="http://schemas.google.com/g/2005#kind"
        term="http://schemas.google.com/blogger/2008/kind#post" />
      <ns0:id>post-{{ $id }}</ns0:id>
      <ns0:author>
        <ns0:name>admin</ns0:name>
      </ns0:author>
      {{-- <ns0:content type="html">{{ str_replace("\n", ' ', $post['content']) }} --}}
      <ns0:content type="html">{{ str_replace("\n", ' ', sanitize_output($post['content'])) }}
      </ns0:content>
      <ns0:published>{{ $date }}Z</ns0:published>
      <ns0:title type="html">{{ makeTitleFromSlug($post['title']) }}</ns0:title>
      <ns0:link href="http://localhost/wpan/{{ $id }}/" rel="self" type="application/atom+xml" />
      <ns0:link href="http://localhost/wpan/{{ $id }}/" rel="alternate" type="text/html" />
    </ns0:entry>
  @endforeach


</ns0:feed>
