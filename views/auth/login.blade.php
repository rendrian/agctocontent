<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Login</title>
</head>

<body>

  <div class="login-container">
    <form action="" method="POST">
      <div class="form-control">
        <label for="email">Email</label>
        <input type="email" name="email">
      </div>
      <div class="form-control">
        <label for="password">Password</label>
        <input type="password" name="password">
      </div>
    </form>
  </div>

</body>

</html>
