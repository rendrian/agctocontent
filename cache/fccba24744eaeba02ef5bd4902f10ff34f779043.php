<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="/assets/css/style.css">
  <title><?php echo $__env->yieldContent('title'); ?></title>
  <?php echo $__env->yieldPushContent('css'); ?>
  <?php echo $__env->yieldPushContent('header'); ?>
</head>

<body>
  <header>
    <?php if ($__env->exists('admin.header')) echo $__env->make('admin.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  </header>
  <section class="content">
    <?php echo $__env->yieldContent('content'); ?>
  </section>
  <footer>

  </footer>
  <script src="/assets/js/script.js"></script>
  <?php echo $__env->yieldPushContent('js'); ?>
  <?php echo $__env->yieldPushContent('footer'); ?>

</body>

</html>
<?php /**PATH D:\laragon\www\content-generator\views/admin/layout.blade.php ENDPATH**/ ?>