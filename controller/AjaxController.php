<?php

class AjaxController
{


  public static function getBladeSyntax($slug)
  {
    $filePath = DOCUMENT_ROOT . 'views/template/' . $slug . '.blade.php';
    // $data =  fread($filePath,0);
    // print_r($data);
    $data = file_get_contents($filePath);
    Flight::json([
      'success' => true,
      'data' => $data
    ]);
  }

  public static function postBladeSyntax()
  {

    $request = Flight::request()->data;
    try {
      $slug = $request->template;
      $filePath = DOCUMENT_ROOT . 'views/template/' . $slug . '.blade.php';
      file_put_contents($filePath, $request->syntax);
      Flight::json([
        'success' => true,
      ]);
    } catch (\Throwable $th) {
      Flight::json([
        'success' => false,
      ]);
    }
  }
}
