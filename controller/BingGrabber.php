<?php

use DiDom\Document;

class BingGrabber
{
  public static function bing_pdf($keyword)
  {



    $max = 5;

    $finalResult = [];
    for ($i = 1; $i < $max; $i++) {
      $url = 'https://www.bing.com/search?q=filetype%3apdf+' . $keyword . '&first=' . $i;


      // print_r($url);
      // die;
      $data_source  = array(
        'url'           => $url,
        'useragent'     => 'Mozilla/5.0 (compatible; Yahoo! Slurp; http://help.yahoo.com/help/us/ysearch/slurp)',
        'referer'       => '',
        'proxy'          => '',
        'account_proxy'  => '',
        'source'        => 'bing',
      );
      try {
        $html         = Grabbing::curl($data_source);
        $dom = new Document($html);
        $results = $dom->find('ol#b_results')[0];
        foreach ($results->find('li.b_algo') as $li) {
          if ($li->has('a')) {
            $h2_a = $li->find('a')[0];
            $first_p = $li->has('p') ? $li->find('p')[0] : '';
            // $related = $li->has('table') ? $li->find('tr') : [];
            $related = [];

            $relatedItem = array_map(function ($item) {
              $link = $item->find('a');
              return [
                'title' => $link[0]->text(),
                'pdflink' => $link[0]->href
              ];
            }, $related);


            $arrayx =  [
              'title' => $h2_a->text(),
              'pdflink' => $h2_a->href,
              'description' => $li->has('p') ? $first_p->text() : '',
              'related' => $relatedItem,
              // "related_keyword" => $relatedKeyword
            ];


            array_push($finalResult, $arrayx);

            if ($li->has('a')) {
              $h2_a = $li->find('a')[0];
              $first_p =  $li->find('p')[0];
              $related = $li->has('table') ? $li->find('tr') : [];

              $relatedItem = array_map(function ($item) {
                $link = $item->find('a');
                return [
                  'title' => $link[0]->text(),
                  'pdflink' => $link[0]->href
                ];
              }, $related);


              $arrayx =  [
                'title' => $h2_a->text(),
                'pdflink' => $h2_a->href,
                'description' => $first_p->text(),
                'related' => $relatedItem,
                // "related_keyword" => $relatedKeyword
              ];


              array_push($finalResult, $arrayx);
            }
          }
        }
      } catch (\Throwable $th) {
        throw $th;
      }
    }

    return $finalResult;
  }

  public static function bing_execute($keyword, $isPdf = false)
  {


    $keyword = urlencode($keyword);
    $data_source  = array(
      'url'           => 'https://www.bing.com/images/search?qft=+filterui:imagesize-large+filterui:aspect-wide&FORM=HDRSC2&count=50&q=' . $keyword,
      'useragent'     => 'Mozilla/5.0 (compatible; Yahoo! Slurp; http://help.yahoo.com/help/us/ysearch/slurp)',
      'referer'       => '',
      'proxy'          => '',
      'account_proxy'  => '',
      'source'        => 'bing',
    );

    $html         = Grabbing::curl($data_source);

    $getJson = Grabbing::parse('m="', '"', $html);
    $getJson = json_decode($getJson);


    $realResult = [];
    foreach ($getJson as $output) {
      // check if string contain sid
      if (strpos($output, 'sid') !== false) {
        $json =  html_entity_decode($output);
        $json = json_decode($json);
        array_push($realResult, $json);
      }
    }

    $images = array_map(function ($item) {
      return $item->murl;
    }, $realResult);
    $copy = array_map(function ($item) {
      return $item->purl;
    }, $realResult);

    $small = array_map(function ($item) {
      return $item->turl;
    }, $realResult);


    $title = array_map(function ($item) {
      $string = $item->t;
      $string = preg_replace('/[\x00-\x1F\x7F-\xFF]/', '', $string);

      if (strpos($string, '-') !== false) {
        $string = explode('-', $string)[0];
      }

      $string =   str_replace(' ...', '', $string);
      $string =   str_replace('|', '', $string);

      return trim($string);
    }, $realResult);



    $realSize = [];
    $sizes = Grabbing::parse('<div class="img_badges"><span data-tooltip="', '"', $html);
    $sizes = json_decode($sizes);

    $pdfs = [];
    if ($isPdf === '1') {
      $keyword = str_replace("+", " ", $keyword);
      $delete = ['download', 'pdf', 'doc', 'docx'];
      foreach ($delete as $kw) {
        $keyword = str_replace($kw, '', $keyword);
      }
      $keyword = urlencode(trim($keyword));
      // $keyword = str_replace('download','',)
      $pdfs = self::bing_pdf($keyword);
      // di($pdfs);
      if (empty($pdfs)) {
        $pdfs = self::bing_pdf(limit_the_words($keyword, 2));
      }
    }

    // if (config('extra.use_sentence_finder')) {
    //   $sentences = (new SentenceFinder)->exec($keyword);
    // }

    $sentences = [];
    $sentences = (new SentenceFinder)->exec($keyword);


    $finalResult = [];
    $allImages = [];
    for ($i = 0; $i < count($images); $i++) {
      array_push($allImages, [
        'title' => $title[$i],
        'thumb' => $small[$i],
        'image' => $images[$i],
        'size' => $sizes[$i],
        'source' => $copy[$i]
      ]);
    }
    $finalResult['images'] = $allImages;
    $finalResult['pdfs'] = $pdfs;
    $finalResult['sentences'] = $sentences;



    $finalResult = json_decode(json_encode($finalResult));
    return $finalResult;
    // $grabbing     = [
    //   // 'images'  => Grabbing::parse('<a class="thumb" target="_blank" href="', '"', $html),
    //   'images'  => $images,
    //   'url'  => $images,
    //   // 'titles'  => Grabbing::parse('<div class="des">', '</div>', $html),
    //   'titles'  => $title,
    //   // 'small'   => Grabbing::parse('width="230" src="', '&amp', $html),
    //   'small'   => $small,
    //   'thumbnail'   => $small,
    //   // 'copy'    => Grabbing::parse('<a class="tit" target="_blank" href="', '"', $html),
    //   'copy'    => $copy,
    //   // 'size'    => Grabbing::parse('class="fileInfo">', '</div>', $html),
    //   'size'    => $sizes,
    //   "pdfs" => ($isPdf) ? $pdfs : [],
    //   "sentences" => $sentences,


    // ];

    // return $grabbing;
  }
}
