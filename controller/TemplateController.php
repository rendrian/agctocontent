<?php
class TemplateController
{

  public static function index()
  {
    isLogin();

    view('admin.template.index');
  }
  public static function newTemplate()
  {
    isLogin();
    view('admin.template.new');
  }
  public static function createNewTemplate()
  {
    isLogin();
    $request = Flight::request()->data;
    $templateName = trim($request->template_name);
    if ($templateName === '') {
      Flight::redirect('/template/new');
    } else {
      $templatePath = DOCUMENT_ROOT . 'views/template/' . makeSLug($templateName) . '.blade.php';
      if (!is_file($templatePath)) {
        file_put_contents($templatePath, '');
        Flight::redirect('/template');
      } else {
        Flight::redirect('/template/new');
      }
    }
  }
}
