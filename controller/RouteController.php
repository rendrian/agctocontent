<?php


class RouteController
{

  public static function index()
  {
    view('admin.index');
  }
  public static function inject()
  {
    $jobList = JobController::getJobList();


    // Flight::json($jobList);

    view('admin.page.inject', ['joblist' => $jobList]);
  }

  public static function doInject()
  {
    $request = Flight::request()->data;
    $time  = date('Y-m-d H:i:s');
    $id = uuid();
    $data = [
      'id' => $id,
      'name' => $request->name,
      'ready' => 1,
      'prepend' => $request->prepend,
      'append' => $request->append,
      'template' => $request->template,
      'is_pdf' => (bool)$request->is_pdf,
      'keywords' => $request->keywordlist,
      'created_at' => $time,
      'modified_at' => $time,
      // 'keywords' => explode("\r\n", $request->keywordlist),
    ];



    JobController::insertJob($data);
    Flight::redirect('/job-details/' . $id);
  }


  public static function deleteConfirm($slug)
  {
    view('admin.job.delete-confirm', ['slug' => $slug]);
  }

  public static function doDeleteJobGenerate($slug)
  {

    try {
      JobController::deleteJob($slug);
      Flight::redirect('/single-generate');
    } catch (\Throwable $th) {
      throw $th;
      Flight::redirect('/');
    }
  }
  public static function doDeleteJob($slug)
  {

    try {
      JobController::deleteJob($slug);
      Flight::redirect('/inject');
    } catch (\Throwable $th) {
      throw $th;
      Flight::redirect('/');
    }
  }

  public static function jobDetails($slug)
  {
    $data = JobController::getJobLog($slug);
    view('admin.job.details', ['data' => $data]);
  }

  public static function doRunJobDetails($slug)
  {
    $request = Flight::request()->data;
    // print_r($request);
    if ($request->action === 'scrape') {
      $cmd = "php " . DOCUMENT_ROOT . "processQueue.php " . $request->id . "";
      if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {

        pclose(popen("start /B " . $cmd, "w"));
      } else {
        $proc = new BackgroundProcess($cmd);
      }


      // die;


      Flight::redirect('/job-details/' . $slug);
      // print_r($proc);

      // print_r($proc);
    } elseif ($request->action === 'export') {

      $cmd = "php " . DOCUMENT_ROOT . "processExport.php " . $request->id . "";
      if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
        pclose(popen("start /B " . $cmd, "w"));
      } else {
        $proc = new BackgroundProcess($cmd);
      }

      // $proc = new BackgroundProcess($cmd);
      Flight::redirect('/job-details/' . $slug);
    }
  }

  public static function generateSingleHtml($slug)
  {
    header("Content-Type: text/plain");
    $filePath = DOCUMENT_ROOT . 'job/generate/' . $slug . '.json';
    $dataArray = json_decode(file_get_contents($filePath));

    $data = $dataArray->data;

    // echo $template = 'template.' . $datarra->template;
    // die;

    $title = trim($dataArray->prepend . ' ' . $dataArray->keyword . ' ' . $dataArray->append);
    view('template.' . $dataArray->template, ['data' => $data, 'title' => $title], true);
  }
  public static function generateSingle()
  {

    $jobList = JobController::getGenerateList();
    view('admin.page.generate-single', ['joblist' => $jobList]);
  }
  public static function doGenerateSingle()
  {
    $request = Flight::request()->data;
    JobController::goGenerateSingle($request);

    // Flight::json($request);
  }

  /**
   * export a single job function
   *
   * @param [string] $slug
   * @return void
   */
  public static function jobExport($slug, $type = 'blogger')
  {

    $request = Flight::request()->data;

    // header("Content-Type:text/xml");
    // $filename = DOCUMENT_ROOT . 'job/queue/' . $slug . '.json';
    // $config = json_decode(file_get_contents($filename));
    $data = db()->from('jobs')->where('id', $slug)->fetch();
    $config = arrayToObject($data);

    $config->ready = 3;
    $template = $config->template;
    $folderPath = DOCUMENT_ROOT . 'job/data/' . $slug;


    $scanDir = scandir($folderPath);
    // array_shift($scanDir);
    array_shift($scanDir);
    array_shift($scanDir);

    $finalArray = [];

    foreach ($scanDir as $file) {

      if (strpos($file, '.json') !== false) {
        $array = json_decode(file_get_contents($folderPath . '/' . $file));
      

        try {
          $html = export('template.' . $template, [
            'title' => $file,
            'data' => $array
          ], false);


          $forContent = [
            'title' => $file,
            'content' => $html,
            'config' => $config
          ];
          array_push($finalArray, $forContent);
        } catch (\Throwable $th) {
          continue;
        }
      }
    }

    // print_r($finalArray);


    header("Content-Type:text/xml");


    view('exports.blogger', ['data' => $finalArray]);
  }
}
