<?php


class Grabbing
{

  public function __construct()
  {
  }
  public static function getWebContent($url, $proxy = '', $userAgent = 'auto', $referer = 'auto')
  {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);

    if (!empty($proxy)) {
      curl_setopt($ch, CURLOPT_PROXY, $proxy);
    }

    if ($userAgent == 'auto') {
      if (isset($_SERVER['HTTP_USER_AGENT']) && !empty($_SERVER['HTTP_USER_AGENT'])) {
        curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
      }
    } elseif (!empty($userAgent)) {
      curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);
    }

    if ($referer == 'auto') {
      if (isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER'])) {
        curl_setopt($ch, CURLOPT_REFERER, $_SERVER['HTTP_REFERER']);
      }
    } elseif (!empty($referer)) {
      curl_setopt($ch, CURLOPT_REFERER, $referer);
    }

    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_AUTOREFERER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);

    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
  }

  public static function parseBing($keyword)
  {

    // get just 3 words

    $pecah = explode(' ', $keyword);
    if (count($pecah) > 3) {
      $keyword = implode(' ', array_slice($pecah, 0, 5));
    }
    // $keyword = str_replace(' ', '+', $keyword);
    $keyword = urlencode($keyword);
    $url = "https://www.bing.com/images/search?q=$keyword&count=20&qft=+filterui:aspect-wide+filterui:imagesize-large&FORM=HDRSC2";




    $userAgent = 'Mozilla/4.0 (compatible; MSIE 6.0b; Windows NT 5.1; DigExt)';

    echo $html = self::cur($url, "", $userAgent);
    die;


    $dom = new \DOMDocument('1.0', 'UTF-8');
    @$dom->loadHTML($html);
    $xpath = new \DOMXPath($dom);

    $results = [];

    $blocks = $xpath->query('//div[@class="content"]/div[@class="row"]/div[@class="item"]');



    if (count($blocks) == 0) {
      return json_encode($results);
    }

    foreach ($blocks as $block) {

      // alt

      $meta = $xpath->query('div[@class="meta"]/div[@class="des"]', $block);



      $alt = $meta->item(0)->textContent ?? '';
      $title = $meta->item(0)->nodeValue ?? '';

      // image

      $thumb = $xpath->query('a[@class="thumb"]', $block);
      $image = $thumb->item(0)->getAttribute('href') ?? '';

      // thumbnail

      $thumb = $xpath->query('a[@class="thumb"]/div[@class="cico"]/img', $block);
      $thumbnail = $thumb->item(0)->getAttribute('src') ?? '';
      if (!empty($thumbnail)) {
        $thumbnail = explode('&', $thumbnail);
        $thumbnail = array_shift($thumbnail);
      }

      // source

      $meta = $xpath->query('div[@class="meta"]/a[@class="tit"]', $block);
      $source = ($meta->length > 0) ? $meta->item(0)->getAttribute('href') : '';

      if (!empty($alt) && !empty($image) && !empty($thumbnail) && !empty($source)) {
        $results[] = array(
          'title' => $title,
          'keyword' => $keyword,
          'alt' => $alt,
          'url' => $image,
          'image' => $image,
          'thumbnail' => $thumbnail,
          'source' => $source
        );
      }
    }

    return $results;
  }



  public static function parse($_start, $_end, $HTML)
  {
    $_start = str_replace("'", "\'", $_start);
    $_start = str_replace(" ", "[^>]+", $_start);
    $_end   = str_replace("'", "\'", $_end);
    $_end   = str_replace(" ", "[^>]+", $_end);
    $parser = preg_match_all('~' . $_start . '\K.*(?=' . $_end . ')~Uis', $HTML, $output);


    return json_encode($output[0]);
  }

  public static function curl($DATA = array())
  {
    if (!isset($DATA['url'])) {
      return false;
    }
    $data = curl_init();
    $header[0] = "Accept: text/xml,application/xml,application/xhtml+xml,";
    $header[0] .= "text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
    $header[] = "Cache-Control: max-age=0";
    $header[] = "Connection: keep-alive";
    $header[] = "Keep-Alive: 300";
    $header[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
    $header[] = "Accept-Language: en-us,en;q=0.5";
    $header[] = "Pragma: ";
    curl_setopt($data, CURLOPT_SSL_VERIFYHOST, FALSE);
    curl_setopt($data, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($data, CURLOPT_URL, $DATA['url']);
    if (isset($DATA['useragent']) && !empty($DATA['useragent'])) {
      curl_setopt($data, CURLOPT_USERAGENT, $DATA['useragent']);
    }
    curl_setopt($data, CURLOPT_HTTPHEADER, $header);
    if (isset($DATA['referer']) && !empty($DATA['referer'])) {
      curl_setopt($data, CURLOPT_REFERER, $DATA['referer']);
    }
    if (isset($DATA['proxy']) && !empty($DATA['proxy'])) {
      curl_setopt($data, CURLOPT_PROXY, $DATA['proxy']);
    }
    if (isset($DATA['account_proxy']) && !empty($DATA['account_proxy'])) {
      curl_setopt($data, CURLOPT_PROXYUSERPWD, $DATA['account_proxy']);
    }
    if (isset($DATA['post']) && !empty($DATA['post'])) {
      curl_setopt($data, CURLOPT_POSTFIELDS, $DATA['post_data']);
      curl_setopt($data, CURLOPT_POST, 1);
    }
    curl_setopt($data, CURLOPT_ENCODING, 'gzip,deflate');
    curl_setopt($data, CURLOPT_AUTOREFERER, true);
    curl_setopt($data, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($data, CURLOPT_CONNECTTIMEOUT, 60);
    curl_setopt($data, CURLOPT_TIMEOUT, 60);
    curl_setopt($data, CURLOPT_MAXREDIRS, 7);
    curl_setopt($data, CURLOPT_FOLLOWLOCATION, true);
    if (isset($DATA['source']) && !empty($DATA['source'])) {
      curl_setopt($data, CURLOPT_COOKIEJAR, $_SERVER['DOCUMENT_ROOT'] . '/cache/cookies_' . $DATA['source'] . '.txt');
      curl_setopt($data, CURLOPT_COOKIEFILE, $_SERVER['DOCUMENT_ROOT'] . '/cache/cookies_' . $DATA['source'] . '.txt');
    }

    $render_output = curl_exec($data);

    if (isset($DATA['minify']) && $DATA['minify'] === true) {
      $render_output = sanitize_output($render_output);
    }

    if (isset($DATA['header'])) {
      $status_header_output = curl_getinfo($data, CURLINFO_HTTP_CODE);
    }
    curl_close($data);
    if (isset($DATA['header'])) {
      return array(
        'HTTP_OUTPUT' => $render_output,
        'HTTP_HEADER' => $status_header_output,
      );
    } else {
      return $render_output;
    }
  }
}
